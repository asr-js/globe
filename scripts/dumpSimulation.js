//
// Creates an offline dump of the isi simulations
//

const request = require("request").defaults({jar: true});
const path = require("path");
const fs = require("fs");

const SERVER_URL = "brunate.isi.it";
const USERNAME = "snat0x54";
const PASSWORD = "cimplex12345";
const SIMULATION_ID = "1484921388913.DFK";
const DESTINATION_FOLDER = path.join(__dirname, "../dataset_new");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

function authenticate(username = USERNAME, password = PASSWORD) {
    return new Promise((resolve, reject) => {
        request.post({
            url: `https://${SERVER_URL}/usr/logon`,
            form: {
                username,
                password
            }
        }, (err, response, body) => {
            if (response.statusCode === 200) {
                resolve();
            } else {
                reject();
            }
        });
    });
}

function getDay(day) {
    return new Promise((resolve, reject) => {
        request(`https://${SERVER_URL}/sim/sim/${SIMULATION_ID}/day/${day}/getabmdata`).on("response", function(response) {
            if (response.statusCode == 200) {
                // from postman: Content-Disposition →attachment; filename=day0001.gdt
                const filename = response.headers["content-disposition"].split("=")[1];
                resolve();
                response.pipe(fs.createWriteStream(path.join(DESTINATION_FOLDER, `${day}`)));
            } else {
                reject();
            }
        });
    });
}

function getDays(currentDay = 1) {
    getDay(currentDay).then(() => {
        console.log("Finished downloading day ", currentDay);
        getDays(currentDay + 1);
    }).catch(() => console.log("Error downloading Day", currentDay));
}

function getSimulationInfo() {
    return new Promise((resolve, reject) => {
        request(`https://${SERVER_URL}/sim/sim/${SIMULATION_ID}/getinfo`).on("response", function(response) {
            if (response.statusCode == 200) {
                response.pipe(fs.createWriteStream(path.join(DESTINATION_FOLDER, "info.xml")));
                resolve();
            } else {
                reject();
            }
        });
    });
}

function getSimulationDefinition() {
    return new Promise((resolve, reject) => {
        request(`https://${SERVER_URL}/sim/sim/${SIMULATION_ID}/getdef`).on("response", function(response) {
            if (response.statusCode == 200) {
                response.pipe(fs.createWriteStream(path.join(DESTINATION_FOLDER, "def.xml")));
                resolve();
            } else {
                reject();
            }
        });
    });
}

function createDestinationFolder() {
    if (!fs.existsSync(DESTINATION_FOLDER)) {
        fs.mkdirSync(DESTINATION_FOLDER);
    }
}

authenticate().then(() => {
    createDestinationFolder();
    getSimulationInfo();
    getSimulationDefinition();
    getDays();
}).catch(() => {
    console.log("Unable to authenticate!");
});
