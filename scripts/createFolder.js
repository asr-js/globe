const path = require("path");
const fs = require("fs-extra");

function createDestinationFolder(folder) {
    if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder);
    }
}

for (let day = 1; day <= 365; ++day) {
  // createDestinationFolder(`./public/sim/sim/abm/${day}`);


  fs.move(`./public/sim/sim/abm/${day}/getdata`, `./public/sim/sim/abm/${day}/getabmdata`, function(err) {
    if(err) {
      console.log(err);
    }
  });
}
