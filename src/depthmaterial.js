/**
 * @author zz85 / https://github.com/zz85 | https://www.lab4games.net/zz85/blog
 *
 * Edge Detection Shader using Frei-Chen filter
 * Based on http://rastergrid.com/blog/2011/01/frei-chen-edge-detector
 *
 * aspect: vec2 of (1/width, 1/height)
 */

const THREE = require("three");

module.exports = {

    uniforms: {

        "tDiffuse": {
            value: null
        },
        "size": {
            value: new THREE.Vector2(1278, 736)
        },
        "filter": {
            value: 0.0
        },
        "cameraNear": {
            type: "f",
            value: 1
        },
        "cameraFar": {
            type: "f",
            value: 10
        }
    },

    vertexShader: `
        #include <common>
        #include <uv_pars_vertex>
        #include <displacementmap_pars_vertex>
        #include <morphtarget_pars_vertex>
        #include <skinning_pars_vertex>
        #include <logdepthbuf_pars_vertex>
        #include <clipping_planes_pars_vertex>
        attribute float visibility;
        varying float vvisibility;

        void main() {

        vvisibility = visibility;

      	#include <uv_vertex>

      	#include <skinbase_vertex>

      	#include <begin_vertex>
      	#include <displacementmap_vertex>
      	#include <morphtarget_vertex>
      	#include <skinning_vertex>
      	#include <project_vertex>
      	#include <logdepthbuf_vertex>
      	#include <clipping_planes_vertex>
        }`,

    fragmentShader: `
        varying float vvisibility;

        #if DEPTH_PACKING == 3200

    	  uniform float opacity;

        #endif

        #include <common>
        #include <packing>
        #include <uv_pars_fragment>
        #include <map_pars_fragment>
        #include <alphamap_pars_fragment>
        #include <logdepthbuf_pars_fragment>
        #include <clipping_planes_pars_fragment>

        void main() {
          if (vvisibility < 0.9) {
              discard;
          }

        	#include <clipping_planes_fragment>

        	vec4 diffuseColor = vec4( 1.0 );

        	#if DEPTH_PACKING == 3200

        		diffuseColor.a = opacity;

        	#endif

        	#include <map_fragment>
        	#include <alphamap_fragment>
        	#include <alphatest_fragment>

        	#include <logdepthbuf_fragment>

        	#if DEPTH_PACKING == 3200

        		gl_FragColor = vec4( vec3( gl_FragCoord.z ), opacity );

        	#elif DEPTH_PACKING == 3201

        		gl_FragColor = packDepthToRGBA( gl_FragCoord.z );

        	#endif

        }`
};
