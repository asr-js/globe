# globe #

A high performance JavaScript library for visualizing data on a globe using WebGL and Fastlane.

### How to build the library? ###
1. Run *npm install*
2. Run *npm run build* or *npm run watch*

### Running the example ###
A more comprehensive example can be found in the public folder. There are two options to run the example

1. Run *npm start* 
2. Run *docker-compose up*

Both options start a web server on http://127.0.0.1:9999

### Setup the library ###
1. Include the public/dist/app.js and public/external/fastlane.js in your HTML document
```
#!html
<script src="dist/app.js"></script>
<script src="external/fastlane.js"></script>
```
2. Example
```
#!javascript
const container = document.getElementById("container");
const globe = new CimplexGlobeView(container, {
	basinsLoaded: globeLoaded
});

function globeLoaded() {
	// update basin values on the globe
	const data = {
		732: 1.2,
		720: 20.3
	};
	globe.updateBasinValues(data);

	// add transitions on the globe
	const transitions = [
		{
			from: 732,
			to: 720,
			weight: 1.0
		}
	];
	globe.addTransitions(transitions);

	// move camera to basin
	globe.moveCameraToBasin(720);
}
```
### API ###

* Create a new globe view instance

```
#!javascript
const globe = new CimplexGlobeView(options) 

options = {
    scaleBasins: Number,         // Scales the basin heights
    enablePostprocessing = true, // Enables/Disables postprocessing
    enableGlobe = true,          // Enables/Disables tiles on the globe
    sphericalMapping = true,     // Enables/Disables 2D/3D projection
    showStats = false,           // Draws stats (fps, triangles)
    basinsHeight = 0.04,         // Setups the maxium height of a basin
    basinsLoaded = undefined     // Callback to a function called when loading of the globe is finished
}
```

* Set basin values

```
#!javascript
	const data = {
		732: 1.2,   // pair of basin id + basin value
		720: 20.3
	};
	globe.updateBasinValues(data);
```

* Add transitions

```
#!javascript
// add transitions on the globe
	const transitions = [
		{
			from: 732,      // source basin
			to: 720,        // target basin
			weight: 1.0     // weight of the basin
		}
	];
	globe.addTransitions(transitions);
```

* Setup tile provider

```
#!javascript
// provide a callback to a custom tile provider
    globeView.urlCallback = (level, x, y) => {
        return `http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/${level}/${y}/${x}`;
    };
```

* Load custom basins/regions

```
#!javascript
fetch("./resources/basins.geojson")
    .then(response => response.json())
    .then(data => {
		// load regions is able to parse custom geojson files
        globeView.loadRegions(data);
    });
```

## Authors

Authors of this project (comprising ideas, architecture, and code) are:

* Sebastian Alberternst <sebastian.alberternst@dfki.de>
* Jan Sutter <jan.sutter@dfki.de>

This project and code was mainly developed by:

* [DFKI](https://www.dfki.de/web/research/asr/index_html) - German Research Center for Artificial Intelligence

Parts of the project and code were developed as part of the [EU H2020](https://ec.europa.eu/programmes/horizon2020/) [project](https://www.cimplex-project.eu/) *CIMPLEX* - Bringing *CI*tizens, *M*odels and Data together in *P*articipatory, Interactive Socia*L* *EX*ploratories.

Futher partners that deliver data and simulations via webservice access are:

* ETHZ (ETH Zurich)
* UCL (University College of London)
* Közép-európai Egyetem (Central European University, CEU)
* ISI (Fondazione Istituto per l'Interscambio Scientifico)
* CNR (National Research Council)
* FBK (Bruno Kessler Foundation)
* USTUTT (University of Stuttgart, Institute for Visualization and Interactive Systems)

## License

See [LICENSE](./LICENSE).
